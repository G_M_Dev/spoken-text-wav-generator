#ifndef WAVNAMER_H
#define WAVNAMER_H

class CWavFileNamer
{
private:
    static CWavFileNamer* m_pcWavNamer;
    unsigned int m_nNbPtrs;
    unsigned int m_nUniqueWavNameNonce;
    
    CWavFileNamer();
    
    void IncrementNonce();

public:
    static CWavFileNamer* GetWavNamer();
    void Release();
    void CraftWavFileName(const std::wstring pszText, std::wstring& pszWav);
};

#endif