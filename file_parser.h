#ifndef FILEPARSER_H
#define FILEPARSER_H

#include <string>
#include <iostream>
#include <vector>

class CFileParser
{
private:
    bool m_bValidFile = false;
    int m_nReaderNdx = -1;
    std::vector<std::wstring> m_vszTexts;
    std::vector<std::wstring> m_vszWAVs;

    std::wstring m_szFolderLocation;

    void AddTextWavPair(std::string szRawLine);
public:
    CFileParser() {};
    CFileParser(const wchar_t* szFileName, bool bUseAREncoding);
    ~CFileParser() {};

    bool ValidFile() { return m_bValidFile; };
    bool GetTextNext(std::wstring& szText);
    bool GetWav(std::wstring& szWav);
    void ResetReader();
};

#endif