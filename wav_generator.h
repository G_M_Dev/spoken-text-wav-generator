#ifndef WAVGENERATOR_H
#define WAVGENERATOR_H

#pragma warning (disable:4996)

#include <iostream>
#include <sapi.h>
#include <sphelper.h>

class CWavGenerator
{
private:
    ISpVoice* m_pVoice = NULL;
    bool m_bVoiceInitialized = false;

    CSpStreamFormat m_cAudioFmt;

public:
    CWavGenerator() {};
    CWavGenerator(ISpVoice* pVoice);
    ~CWavGenerator() {};

    bool MakeWAV(const std::wstring pszText, const std::wstring pszWavFileName);
};

#endif