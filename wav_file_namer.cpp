#include <math.h>
#include <string>
#include <stdlib.h>

#include "wav_file_namer.h"

#define min(a,b) (((a) < (b)) ? (a) : (b))

CWavFileNamer* CWavFileNamer::m_pcWavNamer = NULL;

CWavFileNamer::CWavFileNamer()
{
    m_nUniqueWavNameNonce = 0;
    m_pcWavNamer = this;
    m_nNbPtrs = 1;
}

CWavFileNamer* CWavFileNamer::GetWavNamer() 
{
    if (m_pcWavNamer)   { m_pcWavNamer->m_nNbPtrs++; return m_pcWavNamer; }
    else                { return new CWavFileNamer; }
}

void CWavFileNamer::Release()
{ 
    if (m_nNbPtrs > 1)  { m_nNbPtrs--; } 
    else                { m_nNbPtrs = 0; delete m_pcWavNamer; m_pcWavNamer = NULL; } 
}

void CWavFileNamer::CraftWavFileName(const std::wstring szText, std::wstring& szWav)
{
    // craft szWav
    szWav = szText.substr(0u, 5u) + L"_" + std::to_wstring(m_nUniqueWavNameNonce) + L".wav";
    // increment unique WAV name nonce
    IncrementNonce();
}

void CWavFileNamer::IncrementNonce()
{
    m_nUniqueWavNameNonce++;
}