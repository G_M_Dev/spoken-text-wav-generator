#include <map>

// map filled with values from https://en.wikipedia.org/wiki/Windows-1256
static const std::map<wchar_t, wchar_t> ANSI1256_TO_OEM720 = {
    { 0x0081, 0x067e },
    { 0x008a, 0x0679 },
    { 0x008d, 0x0686 },
    { 0x008e, 0x0698 },
    { 0x008f, 0x0688 },
    { 0x0090, 0x06af },
    { 0x0098, 0x06a9 },
    { 0x009a, 0x0691 },
    { 0x009f, 0x06ba },
    { 0x00a1, 0x060c },
    { 0x00aa, 0x06be },
    { 0x00ba, 0x061b },
    { 0x00bf, 0x061f },    
    { 0x00c0, 0x06c1 },
    { 0x00c1, 0x0621 },
    { 0x00c2, 0x0622 },
    { 0x00c3, 0x0623 },
    { 0x00c4, 0x0624 },
    { 0x00c5, 0x0625 },
    { 0x00c6, 0x0626 },
    { 0x00c7, 0x0627 },
    { 0x00c8, 0x0628 },
    { 0x00c9, 0x0629 },
    { 0x00ca, 0x062a },
    { 0x00cb, 0x062b },
    { 0x00cc, 0x062c },
    { 0x00cd, 0x062d },
    { 0x00ce, 0x062e },
    { 0x00cf, 0x062f },
    { 0x00d0, 0x0630 },
    { 0x00d1, 0x0631 },
    { 0x00d2, 0x0632 },
    { 0x00d3, 0x0633 },
    { 0x00d4, 0x0634 },
    { 0x00d5, 0x0635 },
    { 0x00d6, 0x0636 },
    { 0x00d8, 0x0637 },
    { 0x00d9, 0x0638 },
    { 0x00da, 0x0639 },
    { 0x00db, 0x063a },
    { 0x00dc, 0x0640 },
    { 0x00dd, 0x0641 },
    { 0x00de, 0x0642 },
    { 0x00df, 0x0643 },
    { 0x00e1, 0x0644 },
    { 0x00e3, 0x0645 },
    { 0x00e4, 0x0646 },
    { 0x00e5, 0x0647 },
    { 0x00e6, 0x0648 },
    { 0x00ec, 0x0649 },
    { 0x00ed, 0x064a },
    { 0x00f0, 0x064b },
    { 0x00f1, 0x064c },
    { 0x00f2, 0x064d },
    { 0x00f3, 0x064e },
    { 0x00f5, 0x064f },
    { 0x00f6, 0x0650 },
    { 0x00f8, 0x0651 },
    { 0x00fa, 0x0652 },
    { 0x00ff, 0x06d2 }
};

 inline static void Ansi1256ToOem720(std::wstring& szStr)
 {
     for (int nI = 0; nI < wcslen(szStr.c_str()); nI++) {
         if (ANSI1256_TO_OEM720.count((wchar_t)szStr[nI])) {
             szStr[nI] = ANSI1256_TO_OEM720.at((wchar_t)szStr[nI]);
         }
     }
 }