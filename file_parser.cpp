#include "file_parser.h"

#include <algorithm>
#include <codecvt>
#include <locale>

using namespace std;

#define BUF_SIZE 1024

inline static string utf8_to_string(const char *utf8str, const locale& loc)
{
    // UTF-8 to wstring
    wstring_convert<codecvt_utf8<wchar_t>> wconv;
    wstring wstr = wconv.from_bytes(utf8str);
    // wstring to string
    vector<char> buf(wstr.size());
    use_facet<ctype<wchar_t>>(loc).narrow(wstr.data(), wstr.data() + wstr.size(), '?', buf.data());
    return string(buf.data(), buf.size());
}

// This function assumes the file is valid and exists
inline static void ExtractDirAndNameAndExt(wstring& szDir, wstring& szFileName, wstring& szExt, const wchar_t* szFullPath)
{
    size_t nPathLen = wcslen(szFullPath);
    int nNdx = static_cast<int>(nPathLen-1);
    while ((nNdx>-1) && (szFullPath[nNdx] != '\\') && (szFullPath[nNdx] != '/')) {
        if (szFullPath[nNdx] == '.') { 
            // get the file extension
            wchar_t* szExtAr = (wchar_t*)calloc(nPathLen - nNdx, sizeof(wchar_t)); 
            wcsncpy_s(szExtAr, nPathLen - nNdx, szFullPath + nNdx + 1, nPathLen - nNdx - 1);
            szExt.assign(szExtAr);
            free(szExtAr);
        }
        nNdx--;
    }
    // get the file name
    wchar_t* szFileNameAr = (wchar_t*)calloc(nPathLen - nNdx, sizeof(wchar_t));
    wcsncpy_s(szFileNameAr, nPathLen - nNdx, szFullPath + nNdx + 1, nPathLen - nNdx - 1);
    szFileName.assign(szFileNameAr);
    free(szFileNameAr);
    // get the directory path
    wchar_t* szDirAr = (wchar_t*)calloc(nNdx + 2, sizeof(wchar_t));
    wcsncpy_s(szDirAr, nNdx + 2, szFullPath, nNdx + 1);
    szDir.assign(szDirAr);
    free(szDirAr);
}

void CFileParser::AddTextWavPair(string szRawLine)
{
    wstring szText = L"";
    wstring szWAV = L"";
    bool bDoneWithText = false;
    for (int nNdx = 0; nNdx < szRawLine.length(); nNdx++)
    {
        wchar_t c = (wchar_t)szRawLine[nNdx] & (wchar_t)0x00ff;
        if (c != ',') {
            if (!bDoneWithText) { szText+=c; }
            else                { szWAV+=c; }
        }
        else {
            if (!bDoneWithText) { bDoneWithText = true; }
            else break;
        }
    }
    m_vszTexts.push_back(szText);
    m_vszWAVs.push_back(szWAV);
}

CFileParser::CFileParser(const wchar_t* szFileName, bool bUseAREncoding)
{
    m_bValidFile = false;
    FILE *file; errno_t nFileErr = _wfopen_s(&file, szFileName, L"r");
    char szLine[BUF_SIZE]{ 0 }, szFirstLine[BUF_SIZE]{ 0 }; string szLineConv;
    if ((nFileErr == 0) && file) { // check the file exists...
        // extract file directory path (save it)
        wstring szFile, szExt;
        ExtractDirAndNameAndExt(m_szFolderLocation, szFile, szExt, szFileName);
        // check for CSV extension (print a WARNING otherwise)
        for_each(szExt.begin(), szExt.end(), [](wchar_t & c) {
            c = ::tolower(c);
        });
        if (szExt.compare(L"csv")) { std::wcout << L"WARNING : File extension " << szExt << L" not recognized! Only CSV files are supported..." << std::endl; }
        // read all lines
        locale loc(bUseAREncoding ? ".1256" : ".1252");
        if (fgets(szFirstLine, sizeof(szFirstLine), file) != NULL) {
            szFirstLine[strlen(szFirstLine) - 1] = '\0'; // eat the newline fgets() stores
            if ((szFirstLine[0] == (char)0xef) && (szFirstLine[1] == (char)0xbb)) { 
                strcpy(szLine, szFirstLine + 3); // remove BOM chars at file start
            } 
            szLineConv = utf8_to_string(szLine, loc);
            AddTextWavPair(szLineConv); // extract and save the text/WAV pair
        }
        while (fgets(szLine, sizeof(szLine), file) != NULL)
        {
            szLine[strlen(szLine) - 1] = '\0'; // eat the newline fgets() stores
            szLineConv = utf8_to_string(szLine, loc);
            AddTextWavPair(szLineConv); // extract and save the text/WAV pair
        }
        m_bValidFile = true; // set m_bValidFile to TRUE, if all goes well.
        fclose(file);
    }
    else { // file doesn't exist
        std::wcout << L"ERROR : " << szFileName << L" FAILS TO OPEN! " << _wcserror(nFileErr) << L"! Please provide a valid file path ..." << std::endl;
        return;
    }
}

bool CFileParser::GetTextNext(std::wstring& szText)
{
    if ( m_nReaderNdx < ((int)m_vszTexts.size()-1) ) {
        szText = m_vszTexts[++m_nReaderNdx];
        return true;
    }
    else {
        return false;
    }
}

bool CFileParser::GetWav(std::wstring& szWav)
{
    if ( m_nReaderNdx < (int) m_vszWAVs.size() ) {
        szWav = m_szFolderLocation + m_vszWAVs[m_nReaderNdx];
        return true;
    }
    else {
        return false;
    }
}

void CFileParser::ResetReader()
{
    m_nReaderNdx = -1;
}