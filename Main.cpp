﻿// A simple program that converts a given list of words/phrases into spoken WAV files
/**
USAGE: TextWAVGenerator.exe "voice" ["file"]
ARGS:
- "voice" : corresponds to installed TTS voices (necessary for Voice/Language selection)
- "file"  : optional file listing (word/phrase, WAV name) lines used for WAV files generation
            If the file argument is omitted, the program will look for exs.csv file in current folder by default.
            If exs.csv file is not found, it will prompt the user to enter the (word/phrase, WAV name) pairs one by one.
            When empty word/phrase is entered, the program stops. 
            When empty WAV name is entered, the program names the generated WAV using the first word of the corresponding word/phrase removing diacritics
OUTPUT: a set of WAVs corresponding to entered (word/phrase, WAV name) couples created in same folder as passed "file" (if no "file" arg. passed, then the current directory is used)
**/

#include <string>

#include "TWGVersion.h"

#include "wav_generator.h"
#include "file_parser.h"
#include "wav_file_namer.h"
#include "Ansi1256ToOem720.h"

#define SP1CORECAT_VOICES  L"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech_OneCore\\Voices"
#define MAX_BUFF 255
#define MAX_TXT_BUFF 10*MAX_BUFF

#define WIN_AR_CP 1256
#define WIN_LT_CP 1252

bool IsArabicVoice(ISpObjectToken* cpVoiceToken)
{
    ISpDataKey* cpAttribKey = NULL;
    HRESULT hr = cpVoiceToken->OpenKey(L"Attributes", &cpAttribKey);
    WCHAR* szLang;
    if (SUCCEEDED(hr)) {
        hr = cpAttribKey->GetStringValue(L"Language", &szLang);
    }
    return SUCCEEDED(hr) && (wcscmp(szLang,L"401") == 0);
}

HRESULT GetVoiceName(WCHAR** pszVoiceName, ISpObjectToken* cpVoiceToken, TCHAR* pszVoiceNameAsTChar = NULL)
{
    ISpDataKey* cpAttribKey = NULL;
    HRESULT hr = cpVoiceToken->OpenKey(L"Attributes", &cpAttribKey);
    if (SUCCEEDED(hr)) {
        hr = cpAttribKey->GetStringValue(L"Name", pszVoiceName);
        if (pszVoiceNameAsTChar) {
            WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK | WC_DISCARDNS, *pszVoiceName, -1, pszVoiceNameAsTChar, MAX_BUFF, 0, 0);
        }
    }
    cpAttribKey->Release(); cpAttribKey = NULL;
    return hr;
}

void DisplayAvailableVoices(IEnumSpObjectTokens* cpEnum)
{
    ULONG ulCount = 0;
    // Get the number of voices
    HRESULT hr = cpEnum->GetCount(&ulCount);
    if (FAILED(hr)) {
        std::cout << "ERROR " << hr << ": Failed to get the count of available voices!" << std::endl;
        return;
    }
    // List the available voices by - index : name
    std::cout << "List of voices available on this computer: " << std::endl;
    int nNdx = 0; ISpObjectToken* cpVoiceToken = NULL;
    while (SUCCEEDED(hr) && ulCount--) {
        if (SUCCEEDED(hr)) {
            hr = cpEnum->Next(1, &cpVoiceToken, NULL);
        }
        if (SUCCEEDED(hr)) {
            WCHAR* pszVoiceName = NULL;
            hr = GetVoiceName(&pszVoiceName, cpVoiceToken);
            if (SUCCEEDED(hr)) {
                std::wcout << nNdx << L": " << pszVoiceName << std::endl;
                ::CoTaskMemFree(pszVoiceName);
            }
        }
        cpVoiceToken->Release(); cpVoiceToken = NULL;
        nNdx++;
    }
    if ((LONG)ulCount>=0) {
        std::cout << "ERROR " << hr << ": Failed to enumerate all available voices!" << std::endl;
    }
}

bool FindVoice(IEnumSpObjectTokens* cpEnum, wchar_t* szVoice, ISpObjectToken** cpVoiceToken)
{
    std::wstring szVoiceStr(szVoice); 
    bool bUseNdx = true;
    ULONG ulCount = 0;
    // Get the number of voices
    HRESULT hr = cpEnum->GetCount(&ulCount);
    if (FAILED(hr)) {
        std::cout << "ERROR " << hr << ": Failed to get the count of available voices!" << std::endl;
        return false;
    }
    // Check passed voice is just an index
    size_t nVoiceNdx = 0;
    try {
        nVoiceNdx = std::stoi(szVoiceStr);
    }
    catch (const std::invalid_argument& e) {
        UNREFERENCED_PARAMETER(e);
        bUseNdx = false;
    }
    // Get the voice token
    if (bUseNdx) {
        if (nVoiceNdx < ulCount)
            hr = cpEnum->Item((ULONG)nVoiceNdx, cpVoiceToken);
        else hr = -1;
    }
    else { // Case of voice passed as a string
        bool bMatch = false; int nNdx = 0; ISpObjectToken* cpItVoiceToken = NULL;
        while (SUCCEEDED(hr) && ulCount-- && !bMatch) {
            if (SUCCEEDED(hr)) {
                hr = cpEnum->Next(1, &cpItVoiceToken, NULL);
            }
            if (SUCCEEDED(hr)) {
                WCHAR* pszVoiceName = NULL;
                hr = GetVoiceName(&pszVoiceName, cpItVoiceToken);
                if (SUCCEEDED(hr)) {
                    if (wcscmp(szVoice, pszVoiceName) == 0) { bMatch = true; hr = cpEnum->Item((ULONG)nNdx, cpVoiceToken); }
                    ::CoTaskMemFree(pszVoiceName);
                }
            }
            cpItVoiceToken->Release(); cpItVoiceToken = NULL;
            nNdx++;
        }
        hr = (SUCCEEDED(hr) && bMatch)? S_OK : -1;
        cpEnum->Reset();
    }
    return SUCCEEDED(hr);
}

bool GenerateWAVs(CWavGenerator* pcWavGen, bool bUseAREncoding, wchar_t* szFileName, bool bShowError = false)
{
    bool bSuccess = true;
    CWavFileNamer* pcWavNamer = CWavFileNamer::GetWavNamer();
    CFileParser* pcFileParser = new CFileParser(szFileName, bUseAREncoding); // Open file if any...
    if (pcFileParser->ValidFile()) {
        std::wstring szText, szWav;
        while (pcFileParser->GetTextNext(szText)) { // Parse it...
            if (pcFileParser->GetWav(szWav)) {
                pcWavGen->MakeWAV(szText, szWav); // Generate WAVs from texts...
            }
            else {
                pcWavNamer->CraftWavFileName(szText, szWav);
                pcWavGen->MakeWAV(szText, szWav); // Generate WAVs from texts...
            }
        }
    }
    else {
        if (bShowError) { std::wcout << L"ERROR : Invalid file name: " << szFileName << std::endl; }
        bSuccess = false;
    }
    delete pcFileParser; pcFileParser = NULL;
    pcWavNamer->Release();
    return bSuccess;
}

bool SetConsoleCodePage(ISpObjectToken* cpVoiceToken)
{
    bool bIsAR = IsArabicVoice(cpVoiceToken);
    if (bIsAR) {
        SetConsoleCP(WIN_AR_CP);
        SetConsoleOutputCP(WIN_AR_CP);
    }
    else {
        SetConsoleCP(WIN_LT_CP);
        SetConsoleOutputCP(WIN_LT_CP);
    }
    return bIsAR;
}

int wmain(int argc, wchar_t *argv[])
{
    // Save console I/O code page
    UINT oldicp = GetConsoleCP();
    UINT oldocp = GetConsoleOutputCP();
    // Init COM and the voice object
    if (FAILED(::CoInitialize(0))) {
        std::cout << "ERROR: Failed to initialize COM!" << std::endl;
        return 1;
    }
    ISpVoice* pVoice = NULL;
    HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void**)&pVoice);
    
    // Enumerate voices
    IEnumSpObjectTokens* cpEnum = NULL;
    if (SUCCEEDED(hr)) {
        hr = SpEnumTokens(SP1CORECAT_VOICES, NULL, NULL, &cpEnum);
    }
    else {
        std::cout << "ERROR " << hr <<": Failed to create CLSID_SpVoice COM instance!" << std::endl;
        return 1;
    }

    // Check the command arguments
    if (argc < 2) {
        // report version
        std::wcout << argv[0] << L" Version: " << TWG_VERSION_MAJOR << L"." << TWG_VERSION_MINOR << L"." << TWG_VERSION_PATCH << std::endl;
        std::wcout << std::endl << L"Usage: " << argv[0] << L" \"voice\"   [\"file\"]" << std::endl;
        std::wcout              << L"       " << argv[0] << L" voice_ndx [\"file\"]" << std::endl << std::endl;
        DisplayAvailableVoices(cpEnum);
        return 1;
    }
    
    // Core process
    ISpObjectToken* cpVoiceToken = NULL;
    if (FindVoice(cpEnum, argv[1], &cpVoiceToken)) {
        WCHAR* pszVoiceName = NULL; TCHAR pszVoiceNameAsTChar[MAX_BUFF];
        hr = GetVoiceName(&pszVoiceName, cpVoiceToken, pszVoiceNameAsTChar);
        if (SUCCEEDED(hr)) {
            ::CoTaskMemFree(pszVoiceName);
            std::cout << std::endl << "The requested voice [" << pszVoiceNameAsTChar << "] is successfully found!" << std::endl;
        }
        hr = pVoice->SetVoice(cpVoiceToken);
        if (SUCCEEDED(hr)) {
            std::cout << "The requested voice [" << pszVoiceNameAsTChar << "] is successfully set!" << std::endl;
            CWavGenerator* pcWavGen = new CWavGenerator(pVoice);
            CWavFileNamer* pcWavNamer = CWavFileNamer::GetWavNamer();
            bool bConvertAREncoding = SetConsoleCodePage(cpVoiceToken); // Set console I/O code page
            if (argc > 2) {
                GenerateWAVs(pcWavGen, bConvertAREncoding, argv[2], true);
            }
            else {
                if (!GenerateWAVs(pcWavGen, bConvertAREncoding, L"exs.csv")) {
                    std::wstring szText, szWav;
                    bool bEndPrompt = false;
                    while (!bEndPrompt) {
                        std::cout << "Enter the text to convert: ";
                        std::getline(std::wcin, szText);
                        std::wstring szTextCopy = szText;
                        if (bConvertAREncoding) { Ansi1256ToOem720(szText); }
                        if (!szText.empty()) {
                            std::cout << "Enter the WAV file name: ";
                            std::getline(std::wcin, szWav);
                            if (szWav.empty()) {
                                pcWavNamer->CraftWavFileName(szTextCopy, szWav);
                                std::wstring szWavCopy = szWav;
                                if (bConvertAREncoding) { Ansi1256ToOem720(szWav); }
                                std::wcout << L"No WAV file name entered - the used name is :" << szWavCopy << std::endl;
                            }
                            if (!pcWavGen->MakeWAV(szText, szWav)) // Generate WAVs from texts...
                            {
                                std::wcout << L"ERROR: Failed to generate " << szWav << L" from " << szText << std::endl;
                            }
                        }
                        else { bEndPrompt = true; }
                    }
                }
            }
            delete pcWavGen; pcWavGen = NULL;
            pcWavNamer->Release();
        }
        cpVoiceToken->Release(); cpVoiceToken = NULL;
    }
    else {
        std::cout << std::endl << "The requested voice [" << argv[1] << "] NOT FOUND!" << std::endl << std::endl;
        DisplayAvailableVoices(cpEnum);
        return 1;
    }
    
    // Cleanup
    pVoice->Release(); pVoice = NULL;
    ::CoUninitialize();
    // Reset console I/O code page
    SetConsoleCP(oldicp);
    SetConsoleOutputCP(oldocp);
    return 0;
}
