# Text WAV Generator

Text Wav Generator (TWG) is a commandline tool to convert written text into spoken text saved in `WAV` format.

It takes as inputs: 
- The select Text-to-speech (TTS) voice (installed on Windows / Language Settigns)
- The text to convert
- The WAV file name to create (optional)

The Text and WAV file name inputs can be given in a CSV file listing `text,associated_wav` couples.

## Getting Started

Clone the project to your local. Open the directory CMakeLists.txt in Visual Studio.

The project should compile without any modifications.

### Prerequisites

You need at least:
- One windows TTS arabic voice installed (Language settings) to properly convert arabic text
- One windows TTS english voice installed (Language settings) to properly convert english text
- One windows TTS french voice installed (Language settings) to properly convert french text

### Installing

Select the Configuration to use (`x86-Release`, `x64-Release`, `x64-Debug`).

Update your `Debug and Launch Settings` (`path\to\your\project\clone\TextWAVGenerator\.vs\launch.vs.json`) to pass arguments to the project target.

The commandline feedback is self-explanatory. Running the tool without any arguments shows its current version, its usage and lists all the available voices:

![Tool commandline feedback](tool-commandline-feedback.PNG)

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/Dev-Now/5242959be7da4a9c7c02df361b66bb85) for details on the code of conduct, and the process for submitting merge requests.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/G_M_Dev/spoken-text-wav-generator/-/tags). 

## Authors

* **Ghazi Majdoub** - *Initial work* - [G_M_Dev](https://gitlab.com/G_M_Dev)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to [Stackoverflow](https://stackoverflow.com/) community. I found answers there to almost every single question I had during development.

