#include "wav_generator.h"

CWavGenerator::CWavGenerator(ISpVoice* pVoice)
{
    m_pVoice = pVoice;
    HRESULT hr = m_cAudioFmt.AssignFormat(SPSF_44kHz16BitStereo);
    m_bVoiceInitialized = SUCCEEDED(hr);
    if (FAILED(hr)) {
        std::cout << "ERROR " << hr << ": Failed to initialize WAV Generator utility!" << std::endl;
    }
}

bool CWavGenerator::MakeWAV(const std::wstring pszText, const std::wstring pszWavFileName)
{
    HRESULT hr = -1;
    if (m_bVoiceInitialized) {
        ISpStream* cpStream = NULL;
        hr = SPBindToFile(pszWavFileName.c_str(), SPFM_CREATE_ALWAYS, &cpStream, &m_cAudioFmt.FormatId(), m_cAudioFmt.WaveFormatExPtr());
        if (SUCCEEDED(hr)) { hr = m_pVoice->SetOutput(cpStream, TRUE); }
        if (SUCCEEDED(hr)) { hr = m_pVoice->Speak(pszText.c_str(), SPF_DEFAULT, NULL); }
        if (SUCCEEDED(hr)) { hr = cpStream->Close(); }
        if (SUCCEEDED(hr)) { cpStream->Release(); }
    }
    return SUCCEEDED(hr);
}
